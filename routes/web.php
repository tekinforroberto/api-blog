<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/v1'], function () use ($router) {

    $router->post('/auth/create', 'AuthController@create');
    $router->post('/auth/login', 'AuthController@login');


    $router->group(['middleware' => ['apiJwt']], function () use ($router) {

        $router->post('/auth/logout', 'AuthController@logout');
        $router->post('/auth/refresh', 'AuthController@refresh');

        $router->get('/users', 'UserController@getUsers');
        $router->get('/user/{id}', 'UserController@getUser');
        $router->post('/user', 'UserController@create');
        $router->put('/user/{id}', 'UserController@update');
        $router->get('/user/{id}', 'UserController@delete');


    });








});
