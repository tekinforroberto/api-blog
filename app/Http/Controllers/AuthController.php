<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{


    public function create(Request $request)
    {
        $response = [];

        $validatedData  = $this->validate($request, [
            'name' => ['required', 'min:2'],
            'sobrenome' => ['required'],
            'email' => ['required', 'min:2'],
            'password' => ['required', 'min:8']
        ]);

        $validatedData['password'] = Hash::make($request->password);

        $user = User::create($validatedData);
        $token = Auth::login($user);

        $response['data'] = $user;
        $response['token'] = $this->respondWithToken($token);


        return response()->json($response);
    }

    public function login(Request $request)
    {
        $response = ['error' => ''];

        $credentials = $this->validate($request, [
            'email' => ['email', 'required'],
            'password' => ['required']
        ]);

        if (!$token = Auth::attempt($credentials)) {
            $response['error'] = 'E-mail e/ou senha errados';
            return response()->json($response, 401);
        }

        $response['data'] = auth()->user();
        $response['token'] = $this->respondWithToken($token);

        return response()->json($response, 200);
    }

    public function logout() {
        Auth::logout();

        return response()->json(['menssagem' => 'Logout feito com sucesso.']);
    }

    public function refresh() {
        return $this->respondWithToken(Auth::refresh());
    }

    protected function respondWithToken($token)
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL() * 60
        ];
    }
}
