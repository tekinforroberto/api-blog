<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function getUsers()
    {

        $users = User::paginate(10);

        return response()->json(['data' => $users]);
    }

    public function getUser($id)
    {
        $response = ['error' => ''];

        $user = User::find($id);

        if ($user) {
            $response['data'] = $user;
        } else {
            $response['error'] = 'Usuário não encontrado';
        }

        return response()->json($response);
    }

    public function create(Request $request)
    {
        $response = ['error' => ''];

        $this->validate($request, [
            'name' => ['required', 'min:2'],
            'sobrenome' => ['required', 'min:2'],
            'email' => ['required', 'email', 'unique:users'],
            'password' => ['required', 'min:8']
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->sobrenome = $request->sobrenome;
        $user->email = $request->email;
        $user->password = Hash::make($request->passowrd);
        $user->save();

        $response['data'] = $user;


        return response()->json($response);
    }

    public function update(Request $request, $id)
    {
        $response = ['error' => ''];
        $user = User::find($id);

        if (!$user) {
            return response()->json(['error' => 'Usuário não encontrado.']);
        }

        $this->validate($request, [
            'name' => ['required', 'min:2'],
            'sobrenome' => ['required', 'min:2'],
            'email' => ['required', 'email', Rule::unique('users')->ignore($user->id)],
            'password' => ['required', 'min:8']
        ]);


        $user = new User();
        $user->name = $request->name;
        $user->sobrenome = $request->sobrenome;
        $user->email = $request->email;
        $user->password = Hash::make($request->passowrd);
        $user->save();

        $response['data'] = $user;
        return response()->json($response);
    }

    public function delete($id)
    {
        $response = ['error' => ''];

        $user = User::find($id);

        if ($user) {
            $user->delete();
            $response['sucesso'] = 'Usuário deletado';
        } else {
            $response['error'] = 'Usuário não encontrado';
        }

        return response()->json($response);
    }
}
