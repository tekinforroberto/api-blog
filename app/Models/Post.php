<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    protected $fillable = [
        'user_id',
        'category_id',
        'title',
        'body'
    ];


    public function user() {
        return $this->hasOne(User::class, 'user_id', 'id');
    }

    public function categories() {
        return $this->hasMany(Category::class, 'category_id', 'id');
    }


}
